export $(egrep -v '^#' ../.env | xargs)
docker-compose run --rm wpcli db export --exclude_tables=$(docker-compose run --rm wpcli db tables 'wp_user*' --format=csv) - > wordpress.sql